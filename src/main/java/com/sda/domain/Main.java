package com.sda.domain;

import com.sda.repository.FilmRepository;

public class Main {
    public static void main(String[] args) {
        FilmRepository fr = new FilmRepository();
        System.out.println(fr.findByActorNameAndSurname("KIRSTEN", "PALTROW"));
    }
}
