package com.sda.domain;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Entity
public class Film {

@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
@Column(name = "film_id")
public Short id;

@ManyToMany
@JoinTable(name = "film_category",
        joinColumns = @JoinColumn(name = "film_id"),
        inverseJoinColumns = @JoinColumn(name = "category_id"))
public List<FilmCategory> filmCategories;


public String title;
@Column(name = "rental_rate")
public BigDecimal rentalRate;
public Short length;

@Column(columnDefinition =
        "enum('G', 'PG', 'PG-13', 'R', 'NC-17')")
public String rating;

@ManyToMany(mappedBy = "films")
List<Actor> actors;

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public BigDecimal getRentalRate() {
        return rentalRate;
    }

    public Short getLength() {
        return length;
    }

    public void setRentalRate(BigDecimal rentalRate) {
        this.rentalRate = rentalRate;
    }

    public void setLength(Short length) {
        this.length = length;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Film{");
        sb.append("id=").append(id);
        sb.append(", filmCategories=").append(filmCategories);
        sb.append(", title='").append(title).append('\'');
        sb.append(", rentalRate=").append(rentalRate);
        sb.append(", length=").append(length);
        sb.append('}');
        return sb.toString();
    }
}
