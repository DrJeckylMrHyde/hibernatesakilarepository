package com.sda.repository;

import com.sda.domain.Actor;
import com.sda.domain.Film;
import com.sda.domain.FilmCategory;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class FilmRepository {

     EntityManagerFactory factory = Persistence
            .createEntityManagerFactory("sakila");
     EntityManager entityManager = factory.createEntityManager();

    public Film findById(Short filmId) {
        Film film = entityManager.find(Film.class,filmId);
        return film;
    }

    public List<Film> findFilmsByRating(String rating) {
        return entityManager
                .createQuery("from Film where rating = :rating",Film.class)
//                rating to przekazanie parametru
                .setParameter("rating",rating)
                .getResultList();
    }

    public List<Film> findByActorNameAndSurname(String name, String surname) {
        Actor actor = entityManager
                .createQuery("from Actor where first_name = :name and last_name = :surname", Actor.class)
                .setParameter("name",name)
                .setParameter("surname",surname)
                .getSingleResult();
        return actor.films;
    }

    public List<FilmCategory> findFilmCategories(Short filmId) {
        Film film = entityManager.find(Film.class,filmId);
        return film.filmCategories;
    }
}
