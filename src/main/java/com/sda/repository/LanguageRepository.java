package com.sda.repository;

import com.sda.domain.Language;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class LanguageRepository {

    EntityManagerFactory factory = Persistence
            .createEntityManagerFactory("sakila");
    EntityManager entityManager = factory.createEntityManager();

    public List<Language> findAll() {
        return entityManager.createQuery("from Language",Language.class)
                .getResultList();
    }

}
